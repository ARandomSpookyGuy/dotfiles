#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


#Source
source ~/.alias
source /usr/share/git/git-prompt.sh


#ENV var
export LESS='-R --use-color -Dd+r$Du+b'
export PASSWORD_STORE_CLIP_TIME=240
export EDITOR=nvim
export SUCKLESS=${HOME}/suckless
export FZF_DEFAULT_OPTS="
--exact
--border sharp 
--margin=1 
--height=25 
--color="16,fg:1,preview-fg:4,border:1"
--preview '([[ -f {} ]] && (cat -n {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200'
"
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .password-store  --exclude .git --exclude .gitignore'

#ColorScheme
~/scripts/colorscheme2.sh

#Paths

PATH=$PATH:${HOME}/.config/coc/extensions/coc-clangd-data/install/12.0.0/clangd_12.0.0
PATH="${PATH}:${HOME}/.local/bin/"

#Fzf
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

bind -x '"\C-t":`__fzf_cd__`'


#Tty ColorScheme
#source ~/.cache/wal/colors-tty.sh

#Prompt


function parse_git_branch() {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d ' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		color=242
		NC="\[\e[m\]]"
		echo -n " "
		for i in $(echo $BRANCH | sed -e 's/\(.\)/\1\n/g'); do 
			color=$((color-1))
			branch="\033[01;38;5;${color}m${i}"
			echo -en "${branch}"
		done
		STAT=`parse_git_dirty`
		echo -n "${STAT}"
	else
		echo ""
	fi
}

# get current status of git repo
function parse_git_dirty {
	status=`git status 2>&1 | tee`
	dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
	untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
	ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
	newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
	renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
	deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="*${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}


name(){
	cc=237
	for i in $( whoami | sed -e 's/\(.\)/\1\n/g' );do
		cc=$((cc+1))
		color="\[\033[01;38;5;${cc}m\]"
		no_color="\033[0m"
		echo -en "${color}${i}${no_color}" 
	done
}

directory="\w"

git="\[\e[31m\]\`parse_git_branch\`\[\e[m\]"
brackets_left="\[\e[32m\]\[\e[m\]"
brackets_right="\[\e[32m\]\[\e[m\]"

prompt_="\[\e[32m\]-\[\e[m\]"

if [[ $(tty) == '/dev/tty1' ]];then
	PS1="{\[\e[32m\]\u\[\e[m\]}:{ \w\[\e[31m\]\`parse_git_branch\`\[\e[m\] } -> "
else
	PS1="`name` | $directory$git \n$prompt_ "
fi

